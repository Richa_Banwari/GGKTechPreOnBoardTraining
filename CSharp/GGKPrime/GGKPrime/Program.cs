﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GGKPrime
{
    class Program
    {
        public static int PrimeNo(int number)
        {
            int x = 0;
            
            for(int j=2; j<=Math.Sqrt(number); j++)
            {
                if( number%j==0)
                {
                    x++;
                    break;
                }
            }
            if(x>0)
            {
                return 0;
                
            }
            else
            {
                return 1;

            }
        }

        static void Main(string[] args)
        {
            int maxlimit,k, i;
            int[] fabseries = new int[9999999];
            int[] primefabseries = new int[9999999]; 
                
            Console.WriteLine("Enter the limit :");
            maxlimit = int.Parse(Console.ReadLine());

            fabseries[1] = 1;
            fabseries[2] = 1;

            
            for(  i =3; i<= maxlimit; i++)
            {
                fabseries[i] = fabseries[i - 1] + fabseries[i - 2];
            
            }
            
            Console.WriteLine("Prime Fabonacci Series : ");
            int isprime=1;
            k = 1;

            for (i = 3; i <= maxlimit; i++)
            {
                if (isprime == PrimeNo(i) && isprime == PrimeNo(fabseries[i]) || i == 4)
                {
                    primefabseries[k] = fabseries[i];
                    k++;                   
                }
            }
            int lastterm = 0;
            int arraylength = primefabseries.Length;
           
            for (k=1;k<=arraylength;k++)
            {
                if (primefabseries[k]==0)
                {
                    lastterm = k;
                    
                    break;
                }
            }
            
            for(k =1; k<lastterm ; k++)
            {
                for(int j=1; j<=k; j++)
                {
                    Console.Write("{0} ", primefabseries[j]);
                }
                Console.WriteLine();
            }
            
            Console.ReadLine();
        }
    }
}
