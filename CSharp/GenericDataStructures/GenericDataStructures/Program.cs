﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenericDataStructures
{
    class Program
    {
        //class for stack operations    
        class Stack<T>
        {
            static T[] stack;
            static int topOfStack;
            static int maxStackSize;

            public Stack(int maxSizeOfStack)
            {
                topOfStack = -1;
                maxStackSize = maxSizeOfStack;
                stack = new T[maxSizeOfStack];
            }

            public bool push(T node)
            {

                if (topOfStack < maxStackSize - 1)
                {
                    stack[++topOfStack] = node;
                    return true;
                }
                else
                    return false;
            }
            public void pop()
            {
                if (topOfStack > 0)
                {
                    Console.WriteLine(stack[topOfStack--] + " popped");
                }
                else
                {
                    Console.WriteLine("Stack underflow");
                }

            }

            public void showStack()
            {
                if (topOfStack == -1)
                {
                    Console.WriteLine("Stack empty");
                }
                else
                {
                    foreach (T t in stack)
                    {
                        Console.Write(t + " ");
                    }
                }
            }
        }

        //class for binary node operations
        public class BinNode<T> where T : IComparable
        {
            //constructors
            public BinNode(T data)
            {
                this.data = data;
            }
            public BinNode() { }
            //properties 
            public BinNode<T> right { get; set; } = null;
            public BinNode<T> left { get; set; } = null;
            public T data { get; set; }
        }

        //class for bst operations
        public class BinarySearchTree<T> where T : IComparable
        {
            public BinNode<T> root { get; set; } = null;

            //normal insertion method (inserts T data into a bst instance)
            public void Insert(T data)
            {
                if (root == null)
                {
                    root = new BinNode<T>(data);
                }

                BinNode<T> current = root;

                while (current != null)
                {
                    if (data.CompareTo(current.data) > 0)
                    {
                        if (current.right != null)
                        {
                            current = current.right;
                            continue;
                        }
                        current.right = new BinNode<T>(data);
                    }

                    else if (data.CompareTo(current.data) < 0)
                    {
                        if (current.left != null)
                        {
                            current = current.left;
                            continue;
                        }
                        current.left = new BinNode<T>(data);
                    }

                    else
                    {
                        return;
                    }

                }
            }

            //insert all elements from an array
            public void InsertFromArray(T[] array)
            {
                foreach (T t in array)
                {
                    this.Insert(t);
                }
            }

            //returns the node that holds data equivalent to T data
            public BinNode<T> FindByValue(T data, BinNode<T> node)
            {
                if (node == null) return null;

                if (data.Equals(node.data))
                {
                    return node;
                }

                if (data.CompareTo(node.data) > 0)
                {
                    return FindByValue(data, node.right);
                }
                else if (data.CompareTo(node.data) < 0)
                {
                    return FindByValue(data, node.left);
                }

                else
                {
                    return null;
                }
            }

            //Inorder traversing of nodes
            public void InorderRec(BinNode<T> root)
            {
                if (root != null)
                {
                    InorderRec(root.left);
                    Console.Write(root.data);
                    Console.Write(" ");
                    InorderRec(root.right);
                }
            }

            // Inorder travering of tree
            public void Inorder()
            {
                InorderRec(root);
            }

            //Post order traversing of tree
            public void Postorder()
            {
                PostorderRec(root);
            }

            //Postorder traversing of nodes
            public void PostorderRec(BinNode<T> root)
            {
                if (root != null)
                {
                    PostorderRec(root.right);
                    Console.Write(root.data);
                    Console.Write(" ");
                    PostorderRec(root.left);
                }
            }

        }

         static void Main(string[] args)
            {
                int choice;
            do
            {
                mainChoice: Console.WriteLine("1.Stack\n2.Queue\n3.Binary Search Tree \nEnter your choice(1/2/3) : ");
                switch (int.Parse(Console.ReadLine()))
                {
                    case 1:             //stack operations

                        Console.WriteLine("Enter the max size of stack : ");
                        Stack<string> myStack = new Stack<string>(int.Parse(Console.ReadLine()));
                        stackOps: Console.WriteLine("Stack\n1.Push\n2.Pop\n3.Display stack\nEnter your choice(1/2/3):");
                        choice = int.Parse(Console.ReadLine());
                        switch (choice)
                        {
                            case 1:
                                Console.WriteLine("Enter the value to be entered : ");
                                if (myStack.push(Console.ReadLine()))
                                {
                                    Console.WriteLine("Value pushed");
                                }
                                else
                                {
                                    Console.WriteLine("Stack overflow");
                                }
                                break;
                            case 2:
                                myStack.pop();
                                break;
                            case 3:
                                Console.WriteLine("Content of stack : \n");
                                myStack.showStack();
                                Console.WriteLine();
                                break;
                            default:
                                Console.WriteLine("Wrong choice!\nTry again!\n");
                                goto stackOps;

                        }
                        Console.WriteLine("Do you want to perform any operation again? (y/n): ");
                        if (char.Parse(Console.ReadLine()) == 'y')
                        {
                            goto stackOps;
                        }
                        break;

                    case 2:                  //queue operations
                        Queue<string> myQueue = new Queue<string>();

                        queueOps: Console.WriteLine("Queue\n1.Enter an element\n2.Delete an element\n3.Display all elements\nEnter your choice(1/2/3) : ");
                        switch (int.Parse(Console.ReadLine()))
                        {
                            case 1:
                                Console.WriteLine("Enter the element to be added : ");
                                myQueue.Enqueue(Console.ReadLine());
                                break;
                            case 2:
                                Console.WriteLine(myQueue.Dequeue() + " removed.");
                                break;
                            case 3:
                                foreach (string s in myQueue)
                                {
                                    Console.WriteLine(s);
                                }
                                break;
                            default:
                                Console.WriteLine("Wrong choice!\nTry again!\n");
                                goto queueOps;
                        }
                        Console.WriteLine("Do you want to perform any operation again? (y/n): ");
                        if (char.Parse(Console.ReadLine()) == 'y')
                        {
                            goto queueOps;
                        }
                        break;
                    case 3:        //BST operations
                        {
                            Console.WriteLine("Enter no. of elements to inserted in tree");
                            int maxlimit = int.Parse(Console.ReadLine());

                           bstops: Console.WriteLine("Enter input type \n1 for integer \n2 for float \n3 for string \n4 for char");
                            int inputtype = int.Parse(Console.ReadLine());

                            Console.WriteLine("Enter elements:");

                            switch (inputtype)
                            {
                                case 1:                                //for int input
                                    {
                                        BinarySearchTree<int> bst = new BinarySearchTree<int>();
                                        BinNode<int> binNode = new BinNode<int>();
                                        int[] inputarray = new int[maxlimit];
                                        for (int i = 0; i < maxlimit; i++)
                                        {
                                            inputarray[i] = int.Parse(Console.ReadLine());           //array taken as input
                                        }

                                        bst.InsertFromArray(inputarray);
                                        Console.WriteLine();
                                        Console.WriteLine("Required output in ascending order: ");
                                        bst.Inorder();                                              //in order traversing
                                        Console.WriteLine();
                                        Console.WriteLine("Required output in descending order: ");
                                        bst.Postorder();                                            //post order traversing

                                        Console.WriteLine();
                                        Console.WriteLine("Enter value to be found :");             //search operation on tree
                                        int findvalue = int.Parse(Console.ReadLine());
                                        try
                                        {
                                            Console.WriteLine(bst.FindByValue(findvalue, bst.root).data);
                                            Console.WriteLine("value found");
                                        }
                                        catch (Exception e) { Console.WriteLine("not found"); }
                                        break;
                                    }
                                case 2:                              // for float input
                                    {
                                        BinarySearchTree<float> bst = new BinarySearchTree<float>();
                                        float[] inputarray = new float[maxlimit];
                                        for (int i = 0; i < maxlimit; i++)
                                        {
                                            inputarray[i] = float.Parse(Console.ReadLine());
                                        }

                                        bst.InsertFromArray(inputarray);
                                        Console.WriteLine();
                                        Console.WriteLine("Required output in ascending order: ");
                                        bst.Inorder();
                                        Console.WriteLine();
                                        Console.WriteLine("Required output in descending order: ");
                                        bst.Postorder();

                                        Console.WriteLine();
                                        Console.WriteLine("Enter value to be found :");
                                        float findvalue = float.Parse(Console.ReadLine());
                                        try
                                        {
                                            Console.WriteLine(bst.FindByValue(findvalue, bst.root).data);
                                            Console.WriteLine("value found");
                                        }
                                        catch (Exception e) { Console.WriteLine("not found"); }

                                        break;
                                    }
                                case 3:                                         //for string input
                                    {
                                        BinarySearchTree<string> bst = new BinarySearchTree<string>();
                                        string[] inputarray = new string[maxlimit];
                                        for (int i = 0; i < maxlimit; i++)
                                        {
                                            inputarray[i] = Console.ReadLine();
                                        }

                                        bst.InsertFromArray(inputarray);
                                        Console.WriteLine();
                                        Console.WriteLine("Required output in ascending order: ");
                                        bst.Inorder();
                                        Console.WriteLine();
                                        Console.WriteLine("Required output in descending order: ");
                                        bst.Postorder();

                                        Console.WriteLine();
                                        Console.WriteLine("Enter value to be find :");
                                        string findvalue = Console.ReadLine();
                                        try
                                        {
                                            Console.WriteLine(bst.FindByValue(findvalue, bst.root).data);
                                            Console.WriteLine("value found");
                                        }
                                        catch (Exception e) { Console.WriteLine("not found"); }


                                        break;
                                    }
                                case 4:                                 //for character input
                                    {
                                        BinarySearchTree<char> bst = new BinarySearchTree<char>();
                                        char[] inputarray = new char[maxlimit];
                                        for (int i = 0; i < maxlimit; i++)
                                        {
                                            inputarray[i] = char.Parse(Console.ReadLine());
                                        }

                                        bst.InsertFromArray(inputarray);
                                        Console.WriteLine();
                                        Console.WriteLine("Required output in ascending order: ");
                                        bst.Inorder();
                                        Console.WriteLine();
                                        Console.WriteLine("Required output in descending order: ");
                                        bst.Postorder();

                                        Console.WriteLine();
                                        Console.WriteLine("Enter value to be found :");
                                        char findvalue = char.Parse(Console.ReadLine());
                                        try
                                        {
                                            Console.WriteLine(bst.FindByValue(findvalue, bst.root).data);
                                            Console.WriteLine("value found");
                                        }
                                        catch (Exception e) { Console.WriteLine("not found"); }

                                        break;
                                    }
                                default:
                                    {
                                        Console.WriteLine("Enter appropiate input type");
                                        goto bstops;
                                        break;
                                    }
                            }

                            break;
                        }
                    default:
                        Console.WriteLine("Wrong choice!\nTry again!\n");
                        goto mainChoice;

                }
                Console.WriteLine("Do you want to perform operations again ? (y / n) : ");

            } while (char.Parse(Console.ReadLine()) == 'y');


            Console.ReadLine();
            }
        }
    }

