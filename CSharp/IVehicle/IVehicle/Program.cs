﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IVehicle
{
    class Program
    {
        static void Main(string[] args)
        {
            Program program = new Program();
            Console.WriteLine("Enter Vehicle name to be built :");
            string vehicle = Console.ReadLine();
            program.BuildVehicle(vehicle);
            Console.ReadLine();
        }

        public void BuildVehicle(String vehicle)
        {
            Console.WriteLine("Enter its Number \n1 for bus \n2 for car \n3 for truck:");

            IVehicle factory ;

            int i = int.Parse(Console.ReadLine());
            switch (i)
            {
                case 1:

                    { factory = new Bus();
                        break;
                    }
                case 2:
                    { factory = new Car();
                        break;

                    }
                case 3:
                    { factory = new Truck();
                        break;
                    }
                default:
                    { throw new NotImplementedException(); }
            }
             factory.Build();
        }
    }
    public class Bus : IVehicle
    {
        public override void Build()
        {
            Console.WriteLine("bus built");
        }
    }
    public class Car : IVehicle
    {
        public override void Build()
        {
            Console.WriteLine("car built");
        }
    }
    public class Truck : IVehicle
    {
       public override void Build(  )
        {
            Console.WriteLine("Truck built");
        }
    }
    public abstract class IVehicle
    {
        public static abstract  void Build();   
    }











    /*
    public class ConcreteVehicleFactory: IVehicle
    {
        
        public override IVehicle Build(string vehicle)
        {
            string veh = vehicle;
            switch(veh)
            {
                case Bus:
                    return new Bus();
                case car:
                    return new Bus();
                case truck:
                    return new Bus();
                default:
                    throw new NotImplementedException();
            }

        }
    }
        */

    








}
