﻿using System;

namespace GGKMultiplication
{
    class BinaryOperation
    {
        public string FloatToBinary(decimal decimalNumber)
        {
            if (decimalNumber > 0)
            {
                int intPart = (int)decimalNumber;
                decimal decimalPart = decimalNumber - intPart;
                return "0" + (IntPartConversion(intPart) + DecimalPartConversion(decimalPart)).ToString();
            }
            else
            {
                decimalNumber = decimalNumber * -1;
                int intPart = (int)decimalNumber;
                decimal decimalPar = decimalNumber - intPart;
                return "1" + (IntPartConversion(intPart) + DecimalPartConversion(decimalPar)).ToString();
            }
        }
        
        public decimal IntPartConversion(int intPart)
        {
            string binaryValue = String.Empty;
            while (intPart > 0)
            {
                string temp = (intPart % 2).ToString();
                intPart = intPart / 2;
                binaryValue = binaryValue + temp;
            }
            return ReverseNumber(binaryValue);
        }

        public decimal ReverseNumber(string binaryNumber)
        {
            decimal reversedNumber = 0;

            int length = binaryNumber.Length;

            for (int i = (length - 1); i >= 0; i--)
            {
                reversedNumber = reversedNumber * 10 + int.Parse(binaryNumber[i].ToString());
            }

            return reversedNumber;
        }

        public decimal DecimalPartConversion(decimal decimalPart)
        {
            decimal temp, convertedBinary = 0;

            int length = 0;

            while (length < 5 && decimalPart <= 1)
            {
                decimalPart= decimalPart*2 ;

                if (decimalPart >= 1)
                {
                    temp = (decimal)Math.Pow(10, -length - 1) * 1;
                    convertedBinary = convertedBinary + temp;
                    decimalPart = decimalPart - 1;
                }
                length++;
            }

            return convertedBinary;
        }

        public decimal BinaryToFloat(string number)
        {
            int sign = int.Parse(number.Substring(0, 1));
            number = number.Substring(1);
            decimal binaryNumber = decimal.Parse(number);
            int i, length;
            string intPart;
            string decimalPart;
            decimal temp;

            string[] binaryNumberAfterSplit = binaryNumber.ToString().Split('.');
            intPart = binaryNumberAfterSplit[0];
            try
            {
                decimalPart = binaryNumberAfterSplit[1];
            }
            catch
            {
                decimalPart = String.Empty;
            }

            decimal convertedDecimal = 0;
            length = intPart.Length;

            for (i = 0; i < length; i++)
            {
                temp = int.Parse(intPart[length - i - 1].ToString());
                convertedDecimal = convertedDecimal + temp * (int)Math.Pow(2, i);
            }

            length = decimalPart.Length;
            temp = 0;
            for (i = 0; i < length; i++)
            {
                if (decimalPart[i] == '1')
                {
                    temp += (1 / (decimal)Math.Pow(2, i + 1));
                }
            }
            convertedDecimal = convertedDecimal + temp;
            if (sign == 1)
            {
                convertedDecimal = convertedDecimal * -1;
            }
            return convertedDecimal;
        }
        
        public string Multiply(string number1, string number2)
        {
            int sign1 = int.Parse(number1.Substring(0, 1));
            int sign2 = int.Parse(number2.Substring(0, 1));             
            number1 = number1.Substring(1);
            number2 = number2.Substring(1);
            decimal binary1 = decimal.Parse(number1);
            decimal binary2 = decimal.Parse(number2);
            decimal product = 0;
            int power1 = 0, power2 = 0;
            DecimalPartOperation(ref binary1, ref power1);
            DecimalPartOperation(ref binary2, ref power2);
            int factor = 1;
            while (binary2 > 0)
            {
                if ((binary2 % 10) == 1)
                {
                    decimal temp = (decimal)(binary1 * factor);
                    product = AddBinaryNumbers(product.ToString(), temp.ToString());
                }
                try
                {
                    binary2 = (int)binary2 / 10;
                }
                catch
                {
                    binary2 = decimal.Parse(binary2.ToString().Substring(0, binary2.ToString().Length - 1));
                }
                factor = factor * 10;

            }
            if (power1 + power2 > 0)
            {
                PlaceDecimalPoint(ref product, power1 + power2);
            }

            if (sign1 != sign2)
            {
                return "1" + product.ToString();
            }
            else
            {
                return "0" + product.ToString();
            }
        }
        public void PlaceDecimalPoint(ref decimal number, int power)
        {
            int size = number.ToString().Length;
            string changedNumber = String.Empty;
            changedNumber = number.ToString().Substring(0, size - power);
            changedNumber += ".";
            changedNumber += number.ToString().Substring(size - power);
            number = decimal.Parse(changedNumber);
        }

        public void DecimalPartOperation(ref decimal number, ref int power)
        {
            int size = number.ToString().Length;
            int lengthBeforeDecimal = number.ToString().Split('.')[0].Length;

            if (lengthBeforeDecimal == number.ToString().Length)
            {
                return;
            }
            power = number.ToString().Length - lengthBeforeDecimal - 1;
            string temp = (number * (decimal)Math.Pow(10, power)).ToString().Substring(0, size);
            number = decimal.Parse(temp);
        }

        public decimal AddBinaryNumbers(string number1, string number2)
        {
            if (number1 == "0")
            {
                return decimal.Parse(number2);
            }
            string sum = String.Empty;
            LengthPadding(ref number1, ref number2);
            string rNumber1 = StringReverse(number1.ToString());
            string rNumber2 = StringReverse(number2.ToString());
            char currentNum1, currentNum2, carry = '0';

            for (int i = 0; i < rNumber1.Length; i++)
            {
                currentNum1 = rNumber1[i];
                currentNum2 = rNumber2[i];

                if (currentNum1 == '0' && currentNum2 == '0')
                {
                    if (carry == '0')
                    {
                        sum = sum + "0";
                    }
                    else
                    {
                        sum = sum + "1";
                        carry = '0';
                    }
                }
                else if ((currentNum1 == '1' && currentNum2 == '0') || (currentNum1 == '0' && currentNum2 == '1'))
                {
                    if (carry == '0')
                    {
                        sum = sum + "1";
                    }
                    else
                    {
                        sum = sum + "0";
                        carry = '1';
                    }
                }
                else if (currentNum1 == '1' && currentNum2 == '1')
                {
                    if (carry == '0')
                    {
                        sum = sum + "0";
                        carry = '1';
                    }
                    else
                    {
                        sum = sum + "1";
                        carry = '1';
                    }
                }
            }
            if (carry == '1')
            {
                sum = sum + "1";
                carry = '0';
            }
            sum = StringReverse(sum);
            return decimal.Parse(sum);
        }

        public  void LengthPadding(ref string firstnumber, ref string secondnumber)
        {
            int lengthOfFirst = firstnumber.Length, lengthOfSecond = secondnumber.Length;

            if (lengthOfFirst == lengthOfSecond)
                return;
            else
            {
                if (lengthOfFirst > lengthOfSecond)
                {
                    while (lengthOfFirst != secondnumber.Length)
                    {
                        secondnumber = "0" + secondnumber;
                    }
                }
                else if (lengthOfFirst < lengthOfSecond)
                {
                    while (firstnumber.Length != lengthOfSecond)
                    {
                        firstnumber = "0" + firstnumber;
                    }
                }
            }
        }
        public  string StringReverse(string number)
        {
            string temp = String.Empty;
            for (int i = number.Length - 1; i >= 0; i--)
            {
                temp += number[i];
            }
            return temp;
        }
    }
    class Program
    {
        public static void Main(string[] args)
        {
            BinaryOperation binary = new BinaryOperation();
            decimal number1, number2;
            string binary1, binary2;

                Console.WriteLine("Enter 1st numbers : ");
                number1 = decimal.Parse(Console.ReadLine());
                binary1 = binary.FloatToBinary(number1);
                Console.WriteLine("In Binary Format:\n{0}", binary1);

                Console.WriteLine("Enter 2nd numbers : ");
                number2 = decimal.Parse(Console.ReadLine());
                binary2 = binary.FloatToBinary(number2);
                Console.WriteLine("In Binary format :\n{0}", binary2);

                string result = binary.Multiply(binary1, binary2);

                Console.WriteLine("Product in Binary format: {0}", result);
                Console.WriteLine("Product in Float format : {0}", binary.BinaryToFloat(result));
                Console.ReadLine();
                
            
        }
    }
}