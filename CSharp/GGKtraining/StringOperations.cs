﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GGKtraining
{
     class StringOperations
    {
        private static void Main(string[] args)
        {
            Console.WriteLine("Enter the input string: ");
            String inputstring = Console.ReadLine();
             

            char[] chararray = inputstring.ToCharArray();
            int length = chararray.Length;
            int[] numberarray = new int[length];
            int[] avgarray = new int[length-1];
            char[] primearray = new char[length - 1];
            int y = 0,k,i,x,m,j;
            for (i = 0; i < length; i++)
            {
                k = (int)chararray[i];
                numberarray[i] = k;
            
            }
            for( j=0; j<(length-1); j++)
            {
                x = (numberarray[j] + numberarray[j + 1]) / 2;
                for (m = 2; m < x; m++)
                {
                    if (x % m == 0)
                    {  y++;
                       break;
                    }
                }
                if (y == 0)
                { x++; }
                avgarray[j] = x;
               
                primearray[j] = (char)avgarray[j];               
            }
            String outputstring = new string(primearray);
            Console.WriteLine("Final output string is: "+ outputstring);
            Console.ReadLine();
        }
    }
}
