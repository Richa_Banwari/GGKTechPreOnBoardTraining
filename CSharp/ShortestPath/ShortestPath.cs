﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShortestPath
{
    class ShortestPath
    {
            private static int NO_PARENT = -1;

            private static void dijkstra(int[,] adjacencyMatrix,
                                                 int sourceVertex)
            {
                // int nVertices = adjacencyMatrix.Length;
                int nVertices = 10;

                int[] shortestDistances = new int[nVertices];

                bool[] added = new bool[nVertices];

                for (int currentVertex = 0; currentVertex < nVertices; currentVertex++)
                {
                    shortestDistances[currentVertex] = int.MaxValue;
                    added[currentVertex] = false;
                }

                shortestDistances[sourceVertex] = 0;

                int[] parents = new int[nVertices];

                parents[sourceVertex] = NO_PARENT;

                for (int i = 1; i < nVertices; i++)
                {

                    int nearestVertex = -1;
                    int shortestDistance = int.MaxValue;
                    for (int currentVertex = 0; currentVertex < nVertices; currentVertex++)
                    {
                        if (!added[currentVertex] &&
                            shortestDistances[currentVertex] < shortestDistance)
                        {
                            nearestVertex = currentVertex;
                            shortestDistance = shortestDistances[currentVertex];
                        }
                    }

                    added[nearestVertex] = true;

                    for (int currentVertex = 0; currentVertex < nVertices; currentVertex++)
                    {
                        int edgeDistance = adjacencyMatrix[nearestVertex, currentVertex];

                        if (edgeDistance > 0
                            && ((shortestDistance + edgeDistance) <
                                shortestDistances[currentVertex]))
                        {
                            parents[currentVertex] = nearestVertex;
                            shortestDistances[currentVertex] = shortestDistance + edgeDistance;
                        }
                    }
                }

                printSolution(sourceVertex, shortestDistances, parents);
            }


            private static void printSolution(int sourceVertex,
                                              int[] distances,
                                                int[] parents)
            {
                int nVertices = distances.Length;
                Console.WriteLine("Vertex\t Distance\tPath");

                               Console.Write("\n" + sourceVertex + " -> ");
                Console.Write(nVertices - 1 + " \t ");
                Console.Write(distances[nVertices - 1] + "\t\t");
                printPath(nVertices - 1, parents);
            }

            private static void printPath(int currentVertex,
                                              int[] parents)
            {
                int[] name = { 1, 2, 3, 4, 5, 6, 9, 10, 12, 13 };
                if (currentVertex == NO_PARENT)
                {
                    return;
                }

                printPath(parents[currentVertex], parents);
                if (currentVertex != 9)
                {
                    Console.Write(name[currentVertex] + "-> ");

                }
                else
                {
                    Console.Write(name[currentVertex] + " ");
                }

            }

            public static void Main(String[] args)
            {
                int[,] adjacencyMatrix = new int[,]{
                                               {0,1,2,0,0,0,0,0,0,0},
                                               {1,0,0,0,3,0,0,0,0,0},
                                               {2,0,0,1,0,0,0,0,0,0},
                                               {0,0,1,0,0,2,0,0,8,0},
                                               {0,3,0,0,0,1,4,0,0,0},
                                               {0,0,0,2,1,0,0,0,0,0},
                                               {0,0,0,0,4,0,0,1,3,0},
                                               {0,0,0,0,0,0,1,0,0,3},
                                               {0,0,0,8,0,0,3,0,0,2},
                                               {0,0,0,0,0,0,0,3,2,0}  };

                dijkstra(adjacencyMatrix, 0);
                Console.ReadLine();
            }
    }
}
// For the given Graph, everything is taken as hard-coded

